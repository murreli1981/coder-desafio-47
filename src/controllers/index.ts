import EmployeeController from "./employee.controller.ts";
import services from "../services/index.ts";

export default {
  employeeController: new EmployeeController(services.employeeService),
};
