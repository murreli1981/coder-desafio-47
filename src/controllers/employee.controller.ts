import IController from "../interfaces/IController.ts";
import IEmployee from "../interfaces/IEmployee.ts";
import IService from "../interfaces/IService.ts";

let servicio: any;

export default class EmployeeController implements IController {
  constructor(private service: any) {}

  getAll = (ctx: any, next: Function) => {
    ctx.response.body = this.service.getAll();
  };

  getById = (ctx: any, next: Function) => {
    ctx.response.body = this.service.getById(ctx.params.id);
  };

  create = async (ctx: any, next: Function) => {
    try {
      const result = await ctx.request.body();
      let employee = this.service.create(await result.value);
      ctx.response.body = employee;
    } catch (err) {
      console.error(err);
    }
  };

  update = async (ctx: any, next: Function) => {
    try {
      const id = ctx.params.id;
      let result = await ctx.request.body();
      ctx.response.body = this.service.update(id, await result.value);
    } catch (err) {
      console.error(err);
    }
  };

  delete = (ctx: any, next: Function) => {
    const id = ctx.params.id;
    ctx.response.body = this.service.delete(id);
  };
}
