import IEmployee from "./IEmployee.ts";
import { Context } from "https://deno.land/x/oak/mod.ts";

export default interface IController {
  getAll(ctx: Context, next: Function): void;
  getById(ctx: Context, next: Function): void;
  create(ctx: Context, next: Function): void;
  update(ctx: Context, next: Function): void;
  delete(ctx: Context, next: Function): void;
}
