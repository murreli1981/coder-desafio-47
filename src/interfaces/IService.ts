import IEmployee from "./IEmployee.ts";

export default interface IService {
  getAll(): IEmployee[];
  getById(id: string): IEmployee;
  create(employee: any): IEmployee;
  update(id: string, payload: any): IEmployee;
  delete(id: string): IEmployee;
}
