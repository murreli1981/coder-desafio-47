export default interface IEmployee {
  id: string;
  firstname: string;
  lastname: string;
  age: number;
  address: string;
  phoneNumber: string;
  active: boolean;
}
