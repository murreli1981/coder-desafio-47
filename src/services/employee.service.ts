import { v4 } from "https://deno.land/std@0.117.0/uuid/mod.ts";
import IEmployee from "../interfaces/IEmployee.ts";
import IService from "../interfaces/IService.ts";

export default class EmployeeService implements IService {
  constructor(private db: any[]) {}

  public getAll(): IEmployee[] {
    return this.db;
  }
  public getById(id: any): IEmployee {
    return this.db.find((empl) => empl.id === id);
  }
  public create(employee: any): IEmployee {
    let employeeToCreate: any;
    employeeToCreate = employee;
    employeeToCreate.id = crypto.randomUUID();
    this.db.push(employeeToCreate);
    return employeeToCreate;
  }
  public update(id: any, payload: any): IEmployee {
    let employeeUpdated: any;
    this.db = this.db.map((emp) => {
      if (emp.id === id) {
        employeeUpdated = { ...emp, ...payload };
        return employeeUpdated;
      } else return emp;
    });
    return employeeUpdated;
  }

  public delete(id: any): IEmployee {
    let employeeDeleted: any;
    this.db = this.db.filter((emp) => {
      if (emp.id === id) {
        employeeDeleted = emp;
        return;
      } else return emp;
    });
    return employeeDeleted;
  }
}
