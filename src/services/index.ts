import EmployeeService from "./employee.service.ts";

import db from "../db/db.js";

export default {
  employeeService: new EmployeeService(db),
};
