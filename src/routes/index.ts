import { Router } from "https://deno.land/x/oak/mod.ts";
import EmployeeRouter from "./employee.router.ts";
import controllers from "../controllers/index.ts";
const router = new Router({ prefix: "/employees" });
router.allowedMethods();

export default {
  employeeRouter: EmployeeRouter.setRoutes(
    router,
    controllers.employeeController
  ),
};
