import IController from "../interfaces/IController.ts";

export default class EmployeeRouter {
  static setRoutes(router: any, controller: any): any {
    router
      .get("/", controller.getAll)
      .get("/:id", controller.getById)
      .post("/", controller.create)
      .put("/:id", controller.update)
      .delete("/:id", controller.delete);
    return router;
  }
}
