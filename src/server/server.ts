import { Application, Router } from "https://deno.land/x/oak/mod.ts";
import config from "../config/index.ts";

export default class OakServer {
  private app: Application;
  constructor(router: Router) {
    this.app = new Application();
    this.config(router);
  }

  private config(router: Router) {
    this.app.use(router.routes());
  }

  public async start() {
    console.info(`server running on port ${config.PORT}`);
    await this.app.listen({ port: config.PORT });
  }
}
