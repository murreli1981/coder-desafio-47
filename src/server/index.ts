import OakServer from "./server.ts";
import routers from "../routes/index.ts";
export default {
  server: new OakServer(routers.employeeRouter),
};
