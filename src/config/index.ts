import { config } from "https://deno.land/x/dotenv/mod.ts";

const dotenv = config({ safe: true });

export default {
  PORT: +dotenv.PORT || 3000,
};
