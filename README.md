# coder-desafio-47

Deno

## Estructura del proyecto:

```
.
├── .env
├── .env.example
├── .gitignore
├── .vscode
│   └── settings.json
├── README.md
├── resources
│   └── coder-house-backend.postman_collection.json
└── src
    ├── config
    │   └── index.ts
    ├── controllers
    │   ├── employee.controller.ts
    │   └── index.ts
    ├── db
    │   └── db.js
    ├── index.ts
    ├── interfaces
    │   ├── IController.ts
    │   ├── IEmployee.ts
    │   └── IService.ts
    ├── routes
    │   ├── employee.router.ts
    │   └── index.ts
    ├── server
    │   ├── index.ts
    │   └── server.ts
    └── services
        ├── employee.service.ts
        └── index.ts
```

## Inicio del server

el server de deno se ejecuta corriendo `<path-a-deno>deno run --allow-net --allow-read --allow-env ./src/index.ts` que se encargará de levantar el servidor

## Paths:

- `[GET] /employees` lista todos los empleados
- `[GET] /employees/:id` busca un empleado por id
- `[POST] /employees` guarda un nuevo empleado
- `[PATCH] /employees/:id` actualiza uno o varios valores de un empleado
- `[DEL] /employees/:id` elimina un empleado

### Documentación

#### Postman

en la colección de postman: `resources/coder-house-backend.postman_collection.json`, carpeta: `coder-desafio-47` están los endpoints para probar el crud de `employees` para validarlo rapidamente hay unos empleados precargados.

🚨 Todos los endpoints están con el path `localhost:3000` 🚨
